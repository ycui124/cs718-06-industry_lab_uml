package ictgradschool.industry.lab_uml.ex01;

/**
 * Should represent a Sphere.
 *
 * TODO Implement this class.
 */
public class Sphere {
    private double radius;
    private double[] centroid;
    private static int count;


    public Sphere(double radius, double[] centroid) {
        this.radius = radius;
        this.centroid = centroid;
        this.count++;
    }

    public double getRadius() {
        return radius;
    }

    public void setRadius(double radius) {
        this.radius = radius;
    }

    public double[] getCentroid() {
        return centroid;
    }

    public void setCentroid(double[] centroid) {
        this.centroid = centroid;
    }

    public static int getCount() {
        return count;
    }

    public void setCount(int count) {
        this.count = count;
    }
}
