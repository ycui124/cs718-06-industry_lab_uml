package ictgradschool.industry.lab_uml.ex01;

public class Test {
    public void start(){
        Sphere[] sphs=new Sphere[2];
        fillSpheres(sphs);
        System.out.println(Sphere.getCount());
        System.out.println(surfaceArea(sphs));
        System.out.println(volume(sphs));

    }
    public void fillSpheres(Sphere[] spheres){
        double[] c1={5.0,10.0,9.0};
        spheres[0]=new Sphere(2.0,c1);

        double[] c2={20.0,11.0,8.0};
        spheres[1]=new Sphere(5.0,c2);

        double[] c3={6.0,12.0,19.0};
        //spheres[2]=new Sphere(20,c3);
    }
/*    public int getCount(Sphere[] spheres){
        int count=0;
        for (int i = 0; i <spheres.length ; i++) {

                count = spheres[i].getCount();

        }
        return count;
    }*/
    //    Surface Area=4πr2
    public double surfaceArea(Sphere[] spheres){
        double radius=0.0;
        for (int i = 0; i <spheres.length ; i++) {
            radius=spheres[i].getRadius();
        }
        return 4*Math.PI*Math.pow(radius,2);
    }
    //    Volume=4/3 πr3
    public double volume(Sphere[] spheres){
        double radius=0.0;
        for (int i = 0; i <spheres.length ; i++) {
            radius=spheres[i].getRadius();
        }
        return 4*Math.PI*Math.pow(radius,3)/3;
    }
    public static void main(String[] args) {
        Test t=new Test();
        t.start();
//        Sphere s=new Sphere(2,);
//        s.count=2;
//        s.radius=6;
//        s.centroid=new double[3];
//        s.centroid[0]=5;
//        s.centroid[1]=7;
//        s.centroid[2]=9;
//        System.out.println(s.count());
//        System.out.println(s.surfaceArea()*s.count);
//        System.out.println(s.volume()*s.count);
    }
}
